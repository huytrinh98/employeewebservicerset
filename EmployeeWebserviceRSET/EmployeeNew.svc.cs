﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceRSET
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeNew" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeNew.svc or EmployeeNew.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployee
    {
        DataClasses1DataContext data = new DataClasses1DataContext();
        public bool AddEmployee(Employee1 eml)
        {
            try
            {
                data.Employee1s.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteEmployee(int idE)
        {
            throw new NotImplementedException();
        }

        public List<Employee1> GetProductList()
        {
            try
            {
                return (from employee in data.Employee1s select employee).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateEmloyee(Employee1 eml)
        {
            Employee1 employeeToModfy =
                (from employee in data.Employee1s where employee.empID == eml.empID select employee).Single();
            employeeToModfy.Age = eml.address;
            employeeToModfy.address = eml.address;
            employeeToModfy.fistName = eml.fistName;
            employeeToModfy.lastName = eml.lastName;
            data.SubmitChanges();
            return true;

        }
    }
}
